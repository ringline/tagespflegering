<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kindertagespflege Ring - Über mich</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php include 'header.php';?>

    <main id="main">
        <div id="barbara"></div>
        <div class="row">
            <section class="column">
                <h2 class="subtitle">Das bin ich:</h2>
                <ul>
                    <li>
                        Ich bin Jahrgang 1979 und seit September 2000 glücklich verheiratet mit meinem Mann Lars.
                    </li>
                    <li>Die Geburt unserer ersten Tochter 2001 machte uns zu einer kleinen Familie. Inzwischen
                        gehören noch weitere vier Kinder zu unserer Familie, die in den Jahren 2003, 2005, 2008 und 2017 geboren wurden. Außerdem war 2013 bis 2023 war noch ein Pflegesohn (Vollzeit) Teil unserer
                        Familie.
                    </li>
                    <li> 
                        Seit 2003 wohne ich mit meinem Mann und meiner damals noch kleinen Familie in Welzheim.
                    </li>
                    <li>
                        Und was mache ich beruflich? Nach dem Abitur absolvierte ich zuerst ein Studium der Betriebswirtschaftslehre und arbeitete als Wirtschaftsassistentin in einem amerikanischen Unternehmen. 
                    </li>
                    <li>
                        Nach einer längeren "Familienpause" mit verschiedenen Nebenjobs entschied ich mich dann 2007 dazu, Tagesmutter zu werden, und absolvierte die Qualifizierung zur Tagespflegeperson. 2008 ging es dann mit der Kindertagespflege los.
                    </li>
                    <li>
                        Außerdem bin ich seit 2021 qualifizierte Sprachförderkraft und arbeite vormittags in einem Kindergarten.
                    </li>
                </ul>
            </section>
            <section class="column">
                <h2 class="subtitle">Warum Tagesmutter?</h2>
                <p>
                    Mein eigentlicher Beruf hatte nichts mit Kindern zu tun. Allerdings konnte ich schon seit meinem
                    13. Lebensjahr viel Erfahrung mit Kinderbetreuung (Babysitting, Jungschararbeit) sammeln. Das Thema hat
                    mich seither nie losgelassen. Für meinen Mann und mich war auch klar, dass wir uns eine große Familie
                    wünschen. Unsere ältesten drei Kinder wurden zeitweise durch eine Tagesmutter betreut, während ich zum Teil sogar Vollzeit arbeitete – damals kam ich das erste Mal mit der Kindertagespflege in Berührung und fand es als abgebende Mutter ein großartiges Konzept. 
                </p>
                <p>
                    Als ich mit unserem vierten Kind schwanger war, entschloss ich mich, die Qualifizierung zur Tagesmutter zu
                    machen. Während der Qualifizierung wuchs meine Begeisterung und mein Wissen. Durch unsere drei Kinder und meine Erfahrung als Babysitter war für mich klar, dass das genau die Tätigkeit ist, die ich mir für meine Zukunft vorstellen kann. Schneller als geplant, und zwar ca. drei Monate vor der Geburt unseres vierten Kindes 2008, kamen meine ersten Tageskinder zu mir, von welchen die Jüngste bis 2017 von mir betreut wurde.
                </p>
                <p>
                    An meiner Arbeit liebe ich es zu sehen, wie die Kinder sich entwickeln, sie in ihrer Entwicklung zu
                    unterstützen, bei Problemen nach Möglichkeit helfen zu können und auch ein Ansprechpartner für
                    die Eltern zu sein und ihnen bei eventuellen Fragestellungen weiterhelfen zu können, soweit es
                    meine Möglichkeiten erlauben.
                </p>
            </section>
        </div>
    </main>
    <?php include 'footer.php';?>
    <script>
        let menu = document.getElementById('main-menu');

        function toggle(e){
            //event.preventDefault();
            menu.classList.toggle('is-open');
        }

        menu.addEventListener('click', toggle);
    </script>
</body>
</html>