<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kindertagespflege Ring - Betreuungszeiten</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include 'header.php';?>

    <main id="main">
      <div class="row">
        <h1>Wie und wann wird betreut?</h1>
        <section class="column">
          <h2 class="subtitle">Betreuungszeiten</h2>
          <p>
            Ich biete eine Betreuung von <strong>Montag bis Freitag</strong> in der Zeit <strong>von 12.00 bis 18.00 Uhr</strong> an. Je nach
            Absprache mit den Eltern können die Kinder auch länger oder früher betreut werden.
          </p>
        </section>
        <section class="column">
          <h2 class="subtitle">Alter und Mischung der betreuten Tageskinder</h2>
          <p>
            Ich betreue <strong>Jungen und Mädchen im Alter von 6 Monaten bis ca. 14 Jahren</strong>.
            Mit der alters- und geschlechtergemischten Gruppe habe ich sehr positive Erfahrungen gemacht, die
            Kinder wachsen sehr schnell zu einer Gruppe mit familienähnlichen Strukturen zusammen.
          </p>
        </section>
        <section class="column">
            <h2 class="subtitle">Krankheitsvertreung und Urlaub</h2>
            <p>
                Bei einem plötzlichen Ausfall der Tagesmutter kümmern sich die Eltern in der Regel selbst um eine Notfallbetreuung für das Kind. Wenn eine längere Ausfallzeit
                anstehen sollte, versuche ich mit Hilfe unseres Tagesmütter-Vereins eine Vertretung zu organisieren.
            </p>
            <p>
                Die Eltern bekommen im Januar des neuen Jahres meinen Urlaubsplan für das ganze Jahr. Natürlich
                versuche ich, meinen Urlaub so gut es geht mit den Schulferien und den Ferien der Kindergärten
                abzustimmen.
            </p>
          </section>
      </div>
    </main>
    <?php include 'footer.php';?>
    <script>
      let menu = document.getElementById("main-menu");

      function toggle(e) {
        //event.preventDefault();
        menu.classList.toggle("is-open");
      }

      menu.addEventListener("click", toggle);
    </script>
  </body>
</html>
