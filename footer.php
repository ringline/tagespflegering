<footer>
        <div>
          <h3>Impressum</h3>
          Verantwortliche Person: Lars Ring<br />
          Bahnhofstr. 27, 73642 Welzheim, Germany<br />
          Tel.: +491635920889<br />
          info@kindertagespflege-ring.de
        </div>
        <div>
          <h3>In Zusammenarbeit mit:</h3>
          <ul>
            <li>
              <a href="https://www.tamue.de/" target="_blank"
                >Tagesmütter Welzheimer Wald e. V.</a
              >
            </li>
            <li>
              <a href="https://www.rems-murr-kreis.de/jugend-gesundheit-und-soziales/kreisjugendamt/kindertagespflege" target="_blank"
                >Kreisjugendamt Rems-Murr-Kreis - Kindertagespflege</a
              >
            </li>
          </ul>
        </div>
      </footer>