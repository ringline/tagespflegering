<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kindertagespflege Ring - Betreuungszeiten</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include 'header.php';?>

    <main id="main">
      <div class="row">
        <h1>Formales</h1>
        <section class="column">
          <h2 class="subtitle">Formales</h2>
          <p>
            Hier ergänze ich demnächst noch Informationen rund um das Formale zur Kindertagespflege.
          </p>
        </section>
      </div>
    </main>
    <?php include 'footer.php';?>
    <script>
      let menu = document.getElementById("main-menu");

      function toggle(e) {
        //event.preventDefault();
        menu.classList.toggle("is-open");
      }

      menu.addEventListener("click", toggle);
    </script>
  </body>
</html>
