<header id="header" class="page-header">
        <div class="title">
            <div class="logo">
                <a href="index.php"><img src="img/logo_entwurf2.png" width="300" alt="Kindertagespflege Ring"></a>
            </div>
        </div>
        <div id="claim">
            <p>
                Kinder-Tagespflege Barbara Ring - ein Platz auch für Ihr Kind!
            </p>
            <p>
                Qualifizierte Tagespflege für Kinder von 0,5 bis 14 Jahre in Welzheim
            </p>
        </div>

    </header>
    <nav class="menu" id="main-menu">
        <button class="menu-icon">☰ Menu</button>
        <div class="menu-dropdown">
            <ul class="nav-menu">
                <li><a href="index.php">Startseite</a></li>
                <li><a href="about.php">Über mich</a></li>
                <li><a href="kindertagespflege.php">Was ist Kindertagespflege</a></li>
                <li><a href="raeume.php">Meine Räumlichkeiten</a></li>
                <li><a href="betreuungszeiten.php">Wie und wann wird betreut?</a></li>
                <li><a href="kontakt.php">Kontakt</a></li>
              </ul>
        </div>
    </nav>