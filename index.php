<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Kinder-Tagespflege Barbara Ring in Welzheim. Die sympathische Adresse für
    die kompetente und liebevolle Betreuung Ihres Kindes im Rems-Murr-Kreis.">
    <title>Kindertagespflege Barbara Ring in Welzheim</title>
    <link rel="stylesheet" href="style.css">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
</head>
<body>
    <?php include 'header.php';?>
    
    <aside class="hero" id="hero">
    </aside>
    <h1>Kindertagespflege Barbara Ring - Qualifizierte Kinder-Tagespflege in Welzheim</h1>
    <main id="main">
        <div class="row">
            <section class="column">
                <h2 class="subtitle">Alternativ</h2>
                <p>
                    Kinder-Tagespflege ist eine echte Alternative zur Ganztagsbetreuung in der Kita, unter Umständen kostengünstiger, auf jeden Fall persönlicher und flexibler.
                </p>
            </section>
            <section class="column">
                <h2 class="subtitle">Familiär</h2>
                <p>
                    Ihr Kind wird in einer normalen Familie betreut und erlebt die Geborgenheit und Authentizität des "ganz normalen" Familienlebens. Die positiven Erfahrungen, die Ihr Kind in der Tagesfamilie sammelt, sind durch die kleinen Gruppengrößen und das natürliche Umfeld prägender als in einer herkömmlichen Kindertagesstätte und unterstützen die elterliche Erziehung nachhaltig.
                </p>
            </section>
            <section class="column">
                <h2 class="subtitle">Kompetent</h2>
                <p>
                    Als <strong>qualifizierte Tagesmutter</strong> bin ich für diesen Beruf speziell ausgebildet und nehme regelmäßig an weiteren Fortbildungen teil. Mit diesen Qualifikationen und meiner persönlichen Erfahrung als Mutter von fünf Kindern kann ich so eine hohe Betreuungsqualität bieten.
                </p>
            </section>
        </div>
    </main>
    <?php include 'footer.php';?>
    <script>
        let menu = document.getElementById('main-menu');

        function toggle(e){
            //event.preventDefault();
            menu.classList.toggle('is-open');
        }

        menu.addEventListener('click', toggle);
    </script>
</body>
</html>