<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kindertagespflege Ring - Was ist Kindertagespflege?</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include 'header.php';?>
    <main id="main">
      <div class="row">
        <h1>Was ist Kindertagespflege?</h1>
        <section class="column">
          <h2 class="subtitle">Was ist Kindertagespflege?</h2>
          <p>
            Tagespflege ist eine <strong>familienähnliche Betreuungsform</strong>. Das Tageskind
            wird in die Tagesfamilie integriert. Die besonderen individuellen
            Fördermöglichkeiten und die oft hohe zeitliche Flexibilität der
            Tagespflegepersonen sind wesentliche Vorteile der Kindertagespflege.
          </p>
        </section>
        <section class="column">
          <h2 class="subtitle">Gesetzliche Grundlagen</h2>
          <p>
            Das Konzept der Kindertagespflege ist im Sozialgesetzbuch verankert. Hier ein paar Eckpunkte des <a href="https://www.gesetze-im-internet.de/sgb_8/__22.html" target="_blank">§22 SGB VIII</a>:
          </p>
          <ul>
            <li>
                "Tageseinrichtungen sind Einrichtungen, in denen sich Kinder für einen Teil des Tages oder
                ganztägig aufhalten und in Gruppen gefördert werden. Kindertagespflege wird von einer
                geeigneten Kindertagespflegeperson in ihrem Haushalt, im Haushalt des Erziehungsberechtigten
                oder in anderen geeigneten Räumen geleistet."
            </li>
            <li>
                Kindertagespflege soll:
                <ol>
                    <li>die Entwicklung des Kindes zu einer selbstbestimmten, eigenverantwortlichen und
                        gemeinschaftsfähigen Persönlichkeit fördern,</li>
                    <li>die Erziehung und Bildung in der Familie unterstützen und ergänzen,</li>
                    <li>den Eltern dabei helfen, Erwerbstätigkeit, Kindererziehung und familiäre Pflege besser
                        miteinander vereinbaren zu können.</li>
                </ol>
            </li>
            <li>
                Der Förderungsauftrag umfasst Erziehung, Bildung und Betreuung des Kindes und bezieht sich
                auf die soziale, emotionale, körperliche und geistige Entwicklung des Kindes. Er schließt die
                Vermittlung orientierender Werte und Regeln ein. Die Förderung soll sich am Alter und
                Entwicklungsstand, den sprachlichen und sonstigen Fähigkeiten, der Lebenssituation sowie den
                Interessen und Bedürfnissen des einzelnen Kindes orientieren und seine ethnische Herkunft
                berücksichtigen.
            </li>
          </ul>
        </section>
      </div>
    </main>
    <?php include 'footer.php';?>
    <script>
      let menu = document.getElementById("main-menu");

      function toggle(e) {
        //event.preventDefault();
        menu.classList.toggle("is-open");
      }

      menu.addEventListener("click", toggle);
    </script>
  </body>
</html>
