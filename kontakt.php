<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Kindertagespflege Ring - Betreuungszeiten</title>
  <link rel="stylesheet" href="style.css" />
</head>

<body>
    <?php include 'header.php'; ?>


    <main id="main">
    <div class="row">
      <h1>Kontakt</h1>
      <section class="column">
        <h2 class="subtitle">Ihre Nachricht an mich:</h2>
    <?php
        $to = "barbara@ringline.info";
        $betreff = "Neue Kontaktanfrage von kindertagespflege-ring.de";

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $name = isset($_POST['tfName']) ? $_POST['tfName'] : "";
            $email = isset($_POST['tfEmail']) ? $_POST['tfEmail'] : "";
            $nachricht = isset($_POST['tfNachricht']) ? $_POST['tfNachricht'] : "";
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors["email"] = "Bitte geben Sie eine gültige Emailadresse ein!";
            if (empty($name)||empty($nachricht)) $errors["leer"] = "Bitte geben Sie Ihren Namen und eine Nachricht ein!";

            if (empty($errors)) {
                $datum = date('Y-m-d');
                $header="From: $email";
                $header .= "\nContent-type: text/plain; charset=utf-8";
                $text = "Nachricht von $name vom $datum:\n\n";
                $text .= $nachricht;
                if(mail($to, $betreff, $text, $header)){
                    echo "<p class='hinweis'>";
                    echo "Vielen Dank für Ihre Nachricht! Ich werde mich schnellst möglich bei Ihnen melden.";
                    echo "</p>";
                    $name = $email = $nachricht = "";
                } else{
                    echo "<p class='hinweis'>";
                    echo "Das hat leider nicht funktioniert. Bitte senden Sie mir eine Nachricht über info@kindertagespflege-ring.de";
                    echo "</p>";
                };
            } else {
                echo "<p><span class='hinweis'>";
                foreach($errors as $error){
                    echo $error . "\n";
                }
                echo "</span></p>";
            }
        }
    ?>

 

        <form method="post" action="kontakt.php">
          <label for="tfName"><b>*Name:</b></label><br>
          <input type="text" name="tfName" value="<?php if(!empty($name))echo $name; ?>" required><br><br>

          <label for="tfEmail"><b>*E-Mail-Adresse:</b></label><br>
          <input type="text" name="tfEmail" value="<?php if(!empty($email))echo $email; ?>" required><br><br>

          <label for="tfNachricht"><b>*Nachricht:</b></label><br>
          <textarea name="tfNachricht" rows="10" cols="70" required><?php if(!empty($nachricht)) echo $nachricht; ?></textarea><br><br>
          <input type="submit" name="submit">
        </form>
      </section>
    </div>
  </main>
  <?php include 'footer.php'; ?>
  <script>
    let menu = document.getElementById("main-menu");

    function toggle(e) {
      //event.preventDefault();
      menu.classList.toggle("is-open");
    }

    menu.addEventListener("click", toggle);
  </script>
</body>

</html>