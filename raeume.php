<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kindertagespflege Ring - Meine Räumlichkeiten</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include 'header.php';?>
    <main id="main">
      <div class="row">
        <h1>Meine Räumlichkeiten</h1>
        <section class="column">
          <h2 class="subtitle">Wie wir wohnen</h2>
          <p>
            Wir wohnen mitten in der Stadt Welzheim und sind nur wenige Meter
            vom Kinderhaus (Kindertagesstätte) entfernt. Zu Fuß ist das
            Schulzentrum in zehn Minuten zu erreichen, sowie auch sämtliche
            Sporthallen und öffentlichen Einrichtungen. Der Busbahnhof ist in
            ca. zwei Minuten zu Fuß erreichbar. Das Grundstück ist komplett
            eingezäunt. Auf dem Grundstück (ca. 1.000 m²) steht unser Haus mit
            Garten und einem asphaltierten Hof, auf welchem die Kinder mit
            Fahrzeugen aller Art herumfahren können. Im Garten befinden sich ein
            großes Trampolin, ein Sandkasten und ein Schaukelgestell mit
            verschiedenen Schaukeln. Außerdem steht den Kindern ein Fuhrpark mit
            Dreirädern, Bobbycar, Fahrrädern, Laufrädern usw. zur Verfügung.
          </p>
        </section>
        <section class="column">
          <h2 class="subtitle">Die Räumlichkeiten</h2>
          <ul>
            <li>
              Im <strong>Erdgeschoss</strong> befindet sich ein großes
              Spielzimmer (ca. 20 m²), ein Mal- und Bastelzimmer (ca. 10 m²) mit
              Wickelmöglichkeit und ein separates Bad. In diesen Räumlichkeiten findet die hauptsächliche Betreuung der
              Kinder statt. Von dort aus kann man durch die Haustüre oder eine
              Terrassentüre den Garten bzw. den Hof betreten.
            </li>
            <li>
              In der <strong>1. Etage</strong>, welche über ein Treppenhaus
              erreichbar ist, befinden sich ein Wohnzimmer, ein weiteres Bad mit
              Toilette sowie unsere große Wohnküche, in welcher auch die
              Mahlzeiten zubereitet werden oder gemeinsam gekocht und gebacken
              wird. Auch die Hausaufgaben können dort oder im Wohnzimmer gemacht
              werden. Von der Küche aus geht es in das ca. 30 m² große Wohnzimmer mit einem
              großen Esstisch für ca. zehn Personen. Dort werden die Mahlzeiten
              eingenommen und die Schulkinder machen an diesem Tisch ihre
              Hausaufgaben. Vom Wohnzimmer aus gelangt man auf einen großen
              Balkon mit Pflanzen und Sitzgelegenheiten. 
            </li>
            <li>
                Die 2. Etage gehört vollkommen unseren eigenen Kindern und wird von den Tageskindern nicht
                betreten.
            </li>
          </ul>
        </section>
      </div>
    </main>
    <?php include 'footer.php';?>
    <script>
      let menu = document.getElementById("main-menu");

      function toggle(e) {
        //event.preventDefault();
        menu.classList.toggle("is-open");
      }

      menu.addEventListener("click", toggle);
    </script>
  </body>
</html>
