@import url('https://fonts.googleapis.com/css2?family=Bangers&family=Cardo&family=Dancing+Script&family=Karla&family=Luckiest+Guy&family=Montserrat+Alternates&family=Rubik&display=swap');

:root{
    --main-bg-color: #3c6ca8;
    --main-color-pink: #EAB2BB;
}

:root{
    box-sizing: border-box;
    font-size: calc(1vw + 0.6em);
}

*, *::before, *::after{
    box-sizing: inherit;
}

body{
    margin: 0;
    font-family: Rubik, sans-serif;
    background-color: var(--main-bg-color);
    color: white;
}

.title{
    text-align: center;
}

a:link, nav{
    font-weight: bold;
    text-decoration: none;
}

a:visited{
    color: #fcc6cf;    
}

a:hover{
    color: var(--main-color-pink);
    text-decoration: underline;
}

a:active{
    color: white;
}

.page-header{
    padding: 0.4em 1em;
    height: 190px;
}


.title>h1{
    color: #333;
    text-transform: uppercase;
    font-size: 1.5rem;
    margin: 0.2em 0;
}

.hero{
    height: 150px;
    padding: 2.4em 1em;
    text-align: center;
    background-image: radial-gradient(rgba(158, 158, 158, 0.35) 30%,rgba(255, 255, 255, 0.3)), url('img/girl-3194977_1280.jpg');
    background-size: 100%;
    background-position: center;
    font-size: 1.3em;
    color: white;
    text-shadow: 0.1em 0.1em 0.3em black;
}

.title{
    overflow: auto;
}

#claim{
    color: var(--main-color-pink);
    text-align: center;
    padding: 2em;
}

main{
    padding: 0 1em;
    min-height: 110vh;
}

.column{
    border-top: 1px solid #aaa;
}

.subtitle{
    margin-top: 1.5em;
    margin-bottom: 1.5em;
    font-size: 0.875rem;
    text-transform: uppercase;
}

.menu{
    position: relative;
}

.menu-toggle{
    position: absolute;
    top: -3em;
    left: 0em;
    border: 0;
    background-color: transparent;
    width: 3em;
    height: 3em;
    text-indent: 5em;
    white-space: nowrap;
    overflow: hidden;
}

.menu-toggle::after{
    font-size: 3em;
    position: absolute;
    top: 0em;
    left: 0em;
    display: block;
    content: '\2261';
    text-indent: 0;
}

.menu-dropdown{
    display: none;
    position: absolute;
    top: 0.2em;
    left: -1em;
}

.menu.is-open .menu-dropdown{
    display: block;
}

.nav-menu{
    margin: 0;
    padding-left: 0;
    border: 1px solid #aaa;
    border-radius: 0.4em;
    list-style: none;
    background-color: rgb(233,233,233);
}

.nav-menu > li + li{
    border-top: 1px solid #ccc;
}

.nav-menu > li > a{
    display: block;
    padding: 1em 4em 1em 1.4em;
    color: var(--main-bg-color);
    font-weight: normal;
    transition: 0.8s;
}

.nav-menu > li > a:hover{
    background-color: var(--main-bg-color);
    color: var(--main-color-pink);
}

@media only screen and (min-width: 600px){

    .menu-toggle{
        display: none;
    }

    .menu-dropdown{
        display: block;
    }

    .page-header{
        height: 200px;
        width: 100%;
        position: sticky;
        top: 0;
        left: 0;
        background-color: #3C6CA8;
    }

    main{
        max-width: 1000px;
        margin: 0 auto;
    }

    .hero{
        max-width: 1000px;
        padding: 2.8em;
        margin: 0 auto;
    }

    .logo>img{
        float: left;
        width: 300px;
    }

}